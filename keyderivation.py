import hashlib
from Crypto.Cipher import AES

CmaPassPhrase = b"Sri Jayewardenepura Kotte"
CmaKeySeed = b"\xA9\xFA\x5A\x62\x79\x9F\xCC\x4C\x72\x6B\x4E\x2C\xE3\x50\x6D\x38"

def GetKey(aid):
    DerivedKey = aid + CmaPassPhrase
    DerivedKey = hashlib.sha256(DerivedKey).digest()

    alg = AES.new(CmaKeySeed, AES.MODE_ECB)
    DerivedKey = alg.decrypt(DerivedKey)

    return DerivedKey.encode('hex')

