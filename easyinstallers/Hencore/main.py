
import os
import shutil
import tkMessageBox

import defs
from easyinstallers.Hencore import signTo


def run():
    CMA = defs.getCmaDir()
    if not os.path.exists(CMA + "/EXTRACTED/APP/PCSG90096"):
        shutil.copytree(defs.getWorkingDir()+"/easyinstallers/Hencore/PCSG90096" , CMA + "/EXTRACTED/APP/PCSG90096")
    signTo.vp_start_gui()


